#ifndef _PID_H_
#define _PID_H_




float dc_t;


struct dc_pid 			
{
    dc_t ref;
    dc_t feed_back;
    dc_t pre_error;
    dc_t sum_error;
    dc_t p;
    dc_t i;
    dc_t d;
    dc_t out_max;
    dc_t out_min;
};



void dc_pid_init(struct dc_pid *pid,
                 dc_t ref,
                 dc_t p, dc_t i, dc_t d,
                 dc_t out_max, dc_t out_min);



dc_t dc_pid_calc(struct dc_pid *pid);
dc_t dc_pi_calc(struct dc_pid *pid);



#endif