#ifndef _MOTOR_H_
#define _MOTOR_H_

#include "headfile.h"


struct MOTORINFO{
	
	PWMCH_enum pwmchA;
	PWMCH_enum pwmchB;
	
	ADCN_enum adcA;
	ADCN_enum adcB;
	
	INTN_enum 	EncoderpinA;
	PIN_enum 	EncoderpinB;
	
	int32 speed;
	int32 duty;
	u32	 	freq;
}

#endif